---
lang: en
id: index
url: /
title: Fedi Ponies
contributors:
  - Lumière Élevé
---
## Let's go!
### What is Fedi Ponies?
**Fedi Ponies** is a collection of Fediverse instances, mainly aimed towards the My Little Pony fandom, and localized to multiple languages for the ease of everyone. With **Fedi Ponies**, choose your dream instance as your starting community, and open the gateway towards a wider Fediverse!

### What is Fediverse?
Fediverse (federated universe) is an inter-connected network of various separate social networks. Everyone on the Fediverse are able to interact with each other, if without further restrictions applied. It's a lush and organic ensemble of countless communities.

Fediverse is a diverse platform. For Twitter-like microblogging, there are [Mastodon](https://joinmastodon.org), [Misskey](https://misskey.page), [Firefish](https://joinfirefish.org) and etc. [PixelFed](https://pixelfed.org) for Instagram-like image sharing, [PeerTube](https://joinpeertube.org) for YouTube-like video sharing, and so much more.

Despite the instances themselves, by large, Fediverse is not owned by a single entity, thus preventing any possibility of an entity taking over. Unlike corporate-run social media, users on the Fediverse keep the ownership of their own data. It also doesn't possess a drive for profit - the instances mostly relies on donations, making it extremely unlikely to encounter an ad-infested hell.

### How do I join the Fediverse?
It's simple! Choose an instance, then sign up for an account on it. E-mail addresses are often required to pass the verification process, but those emails would end up in the spam folder more often than not, so check your spam folder as well. Once your account gets past admission, welcome to the party!

### Do I have to speak English?
The use of English is encouraged on the Fediverse, but it's neither a requirement nor a moral obligation. You are free to dictate whichever language you want to speak.

### What if I find the default interface unsatisfactory?
In most cases, try installing an app of your own taste! With various excellent Fediverse clients, one definitely would be up in your league. On the browser's side, there are also style sheets and extensions dedicated to beautifying existing interfaces.

## Next steps
Now, what are you waiting for? [Select a server, and join the Fediverse](./next/)!
