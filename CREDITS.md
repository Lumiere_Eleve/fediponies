## Contributors
### Translators
* Bauhinia Inkwell: Traditional Chinese (Hong Kong)
* Karcsesz: Hungarian
* Lumière Élevé: French
* Midnight Ponywka: Russian
* Polak23: Polish
* Shadow Glider: Hebrew
* Sprinkled Frosting: Orthodox Chinese (Taiwan), Simplified Chinese (China)
* Sweet Dreams: Spanish

### Misc
* Feather Icons: `globe.svg`
* NightCandle: Style suggestions
